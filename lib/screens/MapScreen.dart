import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import '../controllers/MapController.dart';
 import '../widgets/OnceFutureBuilder.dart';

class MapScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MapController>(
      builder: (ctx, model, _) => Scaffold(
        body: OnceFutureBuilder(
            future: () async {
              await model.init();
              return await model.getAllUsers();
            } ,
            builder: (ctx, snapShot) {
              if (snapShot.connectionState != ConnectionState.done) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return Stack(children: [

                // Map
                GoogleMap(
                  myLocationButtonEnabled: true,
                  trafficEnabled: false,myLocationEnabled: true,
                  markers: model.markers,
                  circles: model.circles,
                  mapType: MapType.normal,
                  initialCameraPosition: CameraPosition(
                    target: model.getMyLocation(),
                    zoom: 18,
                  ),
                  onMapCreated: (GoogleMapController _controller) async {
                    await model.waitForGoogleMap(_controller);
                    model.mapController = _controller;
                    model.controller.complete(_controller);
                    model.handleCameraZoom();
                  },
                  compassEnabled: true,
                ),

                // List Of Products
                Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                        height: 200,
                        child: ListView.builder(
                          controller: model.productListController,
                          itemBuilder: (ctx, index) {
                            final item = model.sortedUsersList[index];
                            return InkWell(key: Key("${item.id}"),
                              onTap: () => model.moveToMarker(LatLng(
                                  item.address!.geo!.lat!,
                                  item.address!.geo!.lng!)),
                              child: Container(
                                  width: 250,
                                  height: 200,
                                  margin: EdgeInsets.all(8),
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                          height: 100,
                                          width: double.infinity,
                                          child: Image.network(
                                            item.image.toString(),
                                            fit: BoxFit.cover,
                                          )),
                                      Text(
                                        item.name.toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        textAlign: TextAlign.center,
                                      ),
                                      Text(
                                        item.address!.street.toString(),
                                        style: TextStyle(fontSize: 12),
                                        maxLines: 1,
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  )),
                            );
                          },
                          scrollDirection: Axis.horizontal,
                          itemCount: model.sortedUsersList.length,
                        )))
              ]);
            }),
      ),
    );
  }
}
