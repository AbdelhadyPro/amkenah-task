import 'package:amkenah/screens/MapScreen.dart';
import 'package:amkenah/utils/SetupProviders.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';

void main() {

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return  MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
         locale: Locale('ar'),
         home: MapScreen(),
      ),
    );
  }

}

