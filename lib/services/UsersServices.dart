import 'dart:convert';

import 'package:amkenah/services/core/GlobalHttp.dart';
import 'package:amkenah/utils/ApiRoutes.dart';

import '../models/UsersModel.dart';
import 'DummyData.dart';

class UsersServices {
  Future<List<UsersModel>> getAllUsers() async {
    GlobalHttpResponse response = await GlobalHttp.get(ApiRoutes.getAllUsers);
    if (response.statusCode == 200) {
      // return usersModelFromJson(response.body!);
      return usersModelFromJson(json.encode(usersData));
    }else{
      return [];
    }
  }
}
