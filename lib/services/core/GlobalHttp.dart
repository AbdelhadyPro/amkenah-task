import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

class GlobalHttpResponse {
  int? statusCode;
  String? body;

  GlobalHttpResponse({this.statusCode, this.body});
}

// General Http API
class GlobalHttp {
  static Future<GlobalHttpResponse> get(url) async {
    var dio = Dio();

    // bad request adapter
    dioBadRequestAdapter(dio);

    Response? response = await dio.get<String>(
      Uri.encodeFull(url),
      options: Options(
          headers: {"Accept": "application/json"},
          receiveDataWhenStatusError: true,
          followRedirects: false,
          sendTimeout: 5000,
          validateStatus: (stats) {
            return true;
          }),
    );

    String responseBody = response.data;

    return GlobalHttpResponse(
        statusCode: response.statusCode, body: responseBody);
  }

  static void dioBadRequestAdapter(Dio dio) {
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => false;
      return client;
    };
  }
}
