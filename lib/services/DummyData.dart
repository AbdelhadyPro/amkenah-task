List<Map<String,dynamic>> usersData = [
  {
    "id": 1,
    "name": "شقة للبيع في نطاق جسر السويس",
    "username": "Bret",
    "image": "https://read.opensooq.com/wp-content/uploads/2020/03/%D9%86%D8%B5%D8%A7%D8%A6%D8%AD-%D8%B9%D9%86%D8%AF-%D8%B4%D8%B1%D8%A7%D8%A1-%D8%B4%D9%82%D8%A9-%D8%AA%D9%85%D9%84%D9%8A%D9%83-%D9%81%D9%8A-%D8%A7%D9%84%D8%A8%D8%AD%D8%B1%D9%8A%D9%86.jpeg",
    "email": "Sincere@april.biz",
    "address": {
      "street": "جسر السويس",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {"lat": 30.1285915, "lng": 31.3669845}
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 2,
    "name": "فيلا للأيجار مصر الجديدة",
    "username": "Bret",
    "image": "https://apollo-ireland.akamaized.net/v1/files/d9nfslqk2ods-EG/image;s=644x461;olx-st/_1_.jpg",
    "email": "Sincere@april.biz",
    "address": {
      "street": "مصر الجديدة",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {"lat": 30.1123328, "lng": 31.3526055}
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 3,
    "name": "محل للبيع في اول شارع مكرم عبيد ",
    "username": "Bret",
    "image": "http://www.turkeymoon.com/up/uploads/1455035050371.jpg",
    "email": "Sincere@april.biz",
    "address": {
      "street": "مدينة نصر",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {"lat": 30.0619785, "lng": 31.3471744}
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 4,
    "name": "محل للأيجار في مول سيتي سنتر",
    "username": "Bret",
    "image": "https://aqarmasr.s3-eu-west-1.amazonaws.com/listings/1595200980500_710.jpg",
    "email": "Sincere@april.biz",
    "address": {
      "street": "شيراتون المطار",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {"lat": 30.0807282, "lng": 31.3671458}
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 5,
    "name": "شقه للأيجار ميدان الالف مسكن",
    "username": "Bret",
    "image": "https://apollo-ireland.akamaized.net/v1/files/0rmz1x493ws71-EG/image;s=644x461;olx-st/_1_.jpg",
    "email": "Sincere@april.biz",
    "address": {
      "street": "جسر السويس",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {"lat": 30.1189018, "lng": 31.3422539}
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  
] ;
