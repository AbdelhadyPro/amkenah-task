import 'dart:async';
import 'package:amkenah/services/UsersServices.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:marker_icon/marker_icon.dart';

import '../models/UsersModel.dart';

class MapController extends ChangeNotifier {
  Completer<GoogleMapController> controller = Completer();
  double radius = 1000;
  Position? myLocation = null;
  LatLng? staticLocation = const LatLng(30.1285915, 31.3669845);

  late GoogleMapController mapController;
  List<UsersModel> usersList = [];
  List<UsersModel> sortedUsersList = [];
  Set<Marker> markers = <Marker>{};
  Set<Circle> circles = {};


  ScrollController productListController = ScrollController();

  init() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        myLocation = await Geolocator.getLastKnownPosition();
      }
    }

    circles = {Circle(
        circleId: const CircleId("id"),
        center: getMyLocation(),
        radius: radius,strokeWidth: 3
    )};
  }

  Future getAllUsers() async {
    usersList = await UsersServices().getAllUsers();
    for (var element in usersList) {
      if(checkIfLocationInsideCircle(getMyLocation().latitude,getMyLocation().longitude,element.address!.geo!.lat!,element.address!.geo!.lng!)) {
        sortedUsersList.add(element);
      addNewMarker(
          markerId: element.id.toString(),
          lat: element.address!.geo!.lat!,
          lng: element.address!.geo!.lng!,
          name: element.name);
      notifyListeners();
    }
    }
  }

  // Add Marker in Map
  Future<void> addNewMarker(
      {String? markerId, double? lat, double? lng, String? name}) async {
      markers.add(Marker(
        markerId: MarkerId(markerId.toString()),
        icon: await MarkerIcon.circleCanvasWithText(
            text: name.toString(),
            size: Size(500, 100),
            circleColor: Colors.green,
            fontSize: 32,
            fontWeight: FontWeight.w600,
            fontColor: Colors.white),
        position: LatLng(lat!, lng!),
      ));

    notifyListeners();
  }

  Future<void> handleCameraZoom() async {
    List<LatLng> list = [];
    for (var element in usersList) {
      if(checkIfLocationInsideCircle(getMyLocation().latitude, getMyLocation().longitude,element.address!.geo!.lat!,element.address!.geo!.lng!)){
        list.add(LatLng(element.address!.geo!.lat!, element.address!.geo!.lng!));
      }
    }
    LatLngBounds bound = boundsFromLatLngList(list);
    CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 80);
    await mapController.animateCamera(u2);
  }

  LatLngBounds boundsFromLatLngList(List<LatLng> list) {
    assert(list.isNotEmpty);
    double? x0, x1, y0, y1;
    for (LatLng latLng in list) {
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1!) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1!) y1 = latLng.longitude;
        if (latLng.longitude < y0!) y0 = latLng.longitude;
      }
    }
    return LatLngBounds(
        northeast: LatLng(x1!, y1!), southwest: LatLng(x0!, y0!));
  }

  moveToMarker(LatLng latLng) async {
    CameraUpdate u2 = CameraUpdate.newLatLngZoom(latLng, 17);
    await mapController.animateCamera(u2);
  }

  Future<void> waitForGoogleMap(GoogleMapController c) {
    return c.getVisibleRegion().then((value) {
      if (value.southwest.latitude != 0) {
        print(value.southwest.latitude);
        return Future.value();
      }

      return Future.delayed(Duration(milliseconds: 100))
          .then((_) => waitForGoogleMap(c));
    });
  }


  bool checkIfLocationInsideCircle(double circle_x,double circle_y,double meX,double meY) {
    final double distance = Geolocator.distanceBetween(circle_x, circle_y,
        meX, meY);
    if (distance < radius) {
       return true;
    } else {
       return false;
    }
  }

  LatLng getMyLocation() {
    return myLocation != null ?LatLng(myLocation!.latitude,myLocation!.longitude):LatLng(staticLocation!.latitude,staticLocation!.longitude);
  }

}
